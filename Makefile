CXXFLAGS=-std=c++11 -pthread -Og -g

# OpenMPI via postfix, as used on Debian/Ubuntu systems
CXX_OPENMPI=mpicxx.openmpi

# MPICH via postfix, as used on Debian/Ubuntu systems
CXX_MPICH=mpicxx.mpich
# this is a specific version of OpenMPI with ULFM support
CXX_OPENMPI_ULFM=$(shell PATH=$(ULFM_PREFIX)/bin:$(PATH) which mpicxx)
# the latest MPICH alsos support ULFM
CXX_MPICH_ULFM=mpicxx.mpich -DMPIX_FT

TESTS = $(patsubst %.cc,%,$(wildcard *.cc))
COMP = openmpi mpich openmpi-ulfm mpich-ulfm

.PHONY: clean all debug
TESTVERSIONS = $(foreach test,$(TESTS),\
     $(foreach comp,$(COMP),\
		$(test)-$(comp)))



all: $(TESTVERSIONS)
	@echo $(TESTVERSIONS)

debug: CXXFLAGS += -DMPICXX_ENABLE_DEBUGOUTPUT
debug: all

%-openmpi: CXX=$(CXX_OPENMPI)
%-mpich: CXX=$(CXX_MPICH)
%-openmpi-ulfm: CXX=$(CXX_OPENMPI_ULFM)
%-mpich-ulfm: CXX=$(CXX_MPICH_ULFM)

%: CC = $(CXX)

define genobjrule
$(1)-$(2).o: $(1).cc $(1).hh
	@echo '--- Building '$$@
	$$(CXX) -c $$(CXXFLAGS) $(1).cc -o $$@
endef

OBJ = $(patsubst %.hh,%,$(wildcard MPICXX/*.hh))

$(foreach obj,$(OBJ),\
	$(foreach comp,$(COMP),\
		$(eval $(call genobjrule,$(obj),$(comp)))))

define genrule
objects = $(patsubst %.hh,%-$(2).o,$(wildcard MPICXX/*.hh))
%-$(2): objects = $(patsubst %.hh,%-$(2).o,$(wildcard MPICXX/*.hh))
$(1)-$(2).o: $(1).cc $$(objects) Makefile
	@echo '--- Building '$$@
	$$(CXX) -c $$(CXXFLAGS) -o $$@ $$<
$(1)-%: $(1)-%.o
	$$(CXX) $$(CXXFLAGS) -o $$@ $$< $$(objects)
endef

$(foreach test,$(TESTS),\
     $(foreach comp,$(COMP),\
	 	$(eval $(call genrule,$(test),$(comp)))))

clean:
	rm $(foreach comp,$(COMP),\
		*-$(comp) *-$(comp).o MPICXX/*-$(comp).o)
