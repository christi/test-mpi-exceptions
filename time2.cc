#define OMPI_SKIP_MPICXX
#define MPICH_SKIP_MPICXX

#include <unistd.h>
#include <iostream>
#include <sstream>
#include <string>
#include <csignal>
#include <chrono>
#include <iomanip>

#include "MPICXX/instance.hh"
#include "MPICXX/comm.hh"

std::chrono::time_point<std::chrono::system_clock> start;

void printLibraryVersion() {
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (rank == 0) {
        int size;
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        char buf[MPI_MAX_LIBRARY_VERSION_STRING];
        int len;
        int result = MPI_Get_library_version(buf, &len);
        std::string version(buf, len);
        std::string line;
        std::istringstream tmp(version);
        std::getline(tmp, line);
        std::cout << "#" << line << std::endl << "#Using: " << size << " ranks";
#ifdef MPIX_FT
        std::cout << " (ULFM enabled)";
#endif
        std::cout << std::endl;
    }
}

int main(int argc, char** argv) {
    auto & mpi = MPICXX::initialize(argc, argv);
    printLibraryVersion();
    auto& comm_world = mpi.comm_world();
    int rank = comm_world.rank();
    int errortype = 0;
    int iterations = 1;
    if (rank == 0) {
        std::cout  << std::fixed << std::setw(11) << std::setprecision(6);
        if (argc > 1)
            std::istringstream(argv[1]) >> errortype;
    }
    if (argc > 2)
        std::istringstream(argv[2]) >> iterations;
    try {
        auto comm = comm_world.duplicate();
        for (int run = 0; run < iterations; run++) {
            comm_world.raw_barrier();
            start = std::chrono::system_clock::now();  // start time measurement
            try {
                try {
                    if (rank == 0) {
                        if (errortype == 1) {
                            throw(std::runtime_error("Don't Panic!"));
                        } else {
                            for (int i=1; i < comm.size(); ++i) {
                                // send msg to all ranks
                                MPICXX::Future f = comm.send(&errortype, 1,
                                                             MPI_INT, i);
                                f.wait();
                            }
                        }
                    } else {
                        int buf;
                        auto f = comm.recv(&buf, 1, MPI_INT, 0);
                        f.wait();
                    }
                } catch(std::exception& e) {
                    comm.signal_error(42);
                }
            } catch(MPICXX::Propagated_exception& e) {
            }
            comm_world.raw_barrier();
            if (rank == 0) {
                auto end = std::chrono::system_clock::now();
                auto elapsed = end - start;
                std::cout << "\t" <<  static_cast<double>(elapsed.count())/1000000
                          << std::endl;
            }
        }
    }
    catch(...) {
    }
    return 0;
}
