#define OMPI_SKIP_MPICXX
#define MPICH_SKIP_MPICXX


#include <unistd.h>
#include <iostream>
#include <sstream>
#include "MPICXX/instance.hh"
#include "MPICXX/comm.hh"

void test_send_recv(MPICXX::Comm & comm, int P, bool throw_exception) {
    // one to all communication
    if (comm.rank() == P) {
        int i = 10;
        if (throw_exception) {
            usleep(10);
            throw(std::domain_error(std::string("exception on rank ")
                    + std::to_string(comm.rank())));
        }
        {
            MPICXX::Future f[comm.size()];
            for (i = 0; i < comm.size(); i++)
                if (i != P)
                    f[i] = comm.send(&i, 1, MPI_INT, i);
            std::cout << comm.rank() << " issued all send requests"
                      << std::endl;
            for (i = 0; i < comm.size(); i++)
                if (i != P)
                    f[i].wait();
            std::cout << comm.rank() << " all send requests finished"
                      << std::endl;
        }
    } else {
        if (throw_exception && comm.rank() == 0) {
            usleep(10);
            throw(std::domain_error(std::string("second exception on rank ")
                    + std::to_string(comm.rank())));
        }
        int i = 1;
        auto f = comm.recv(&i, 1, MPI_INT, P);
        f.wait();
        std::cout << "recv " << i << std::endl;
    }
}

int main(int argc, char** argv)
try {
    auto & mpi = MPICXX::initialize(argc, argv);
    auto& comm_world = mpi.comm_world();

#ifdef MPIX_FT
    if (comm_world.rank() == 0) std::cout << "ULFM enabled" << std::endl;
#endif
    bool throw_exception = false;
    if (argc > 1)
        std::istringstream(argv[1]) >> std::boolalpha
                                    >> throw_exception;

    if (comm_world.rank() == 0)
        std::cout << "do throw: " << throw_exception << std::endl;

    for (int P=0; P < comm_world.size(); P++) {
        try {
            // create a new communicator, which might die of something goes
            // wrong...
            MPICXX::Comm comm = comm_world.duplicate();
            test_send_recv(comm, P, throw_exception);
        }
        catch (std::exception & e) {
            std::cout << comm_world.rank() << "\tMAIN caught: " << e.what()
                      << std::endl;
            usleep(100);
        } catch(MPICXX::Comm_corrupted_exception ex) {
            std::cout << comm_world.rank() << "\t corrupted communicator!"
                      << std::endl;
        }

        comm_world.raw_barrier();  // this barrier is without error handling!
        if (comm_world.rank() == 0)
            std::cout << "P = " << P << " successful" << std::endl;
    }
    std::cout << comm_world.rank() << "\tMAIN done" << std::endl;
    return 0;
} catch(...) {
     std::cout << "uncaught exception" << std::endl;
     return -1;
}
