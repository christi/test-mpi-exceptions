#ifndef MPICXX_COMM_HH
#define MPICXX_COMM_HH


#include <mpi.h>
#ifdef OPEN_MPI
#include <mpi-ext.h>
#endif
#include <memory>
#include <vector>
#include <cassert>

#include "instance.hh"
#include "exceptions.hh"
#include "future.hh"

#ifdef MPICXX_ENABLE_DEBUGOUTPUT
#include <iostream>
#define DEBUG(A) std::cout << A << std::endl
#else
#define DEBUG(A)
#endif

namespace MPICXX {

enum { ERROR_TAG = 666, COMM_CORRUPTED_TAG = 667 };
class ErrorData;
class Comm;
class Instance;
class Future;

/** \brief wrapper of an MPI communicator */
class Comm {
    friend class Instance;
    friend class Future;
    friend class ErrorData;
    MPI_Comm comm_;
    int rank_;
    int size_;
    std::shared_ptr<ErrorData> error_;
    std::shared_ptr<Instance> mpi_;

    // \brief: creates a new Comm object from an native MPI_Comm
    explicit Comm(const MPI_Comm & comm);

    // \brief: register a new MPI-call and create a future
    template <typename F, typename... Args>
    Future async_call(F mpicall, Args... args);

    // \brief: helper function for checking MPI result codes and
    // Throws the appropriate exception
    void check_mpi_result(int mpi_error_code,
                          const MPI_Status& status = MPI_Status()) const;

 public:
    // \brief: default move-constructor
    Comm(Comm&& comm) = default;

    // \brief: delete copy-contructor
    Comm(const Comm& comm) = delete;

    // \brief: Destructor
    ~Comm();

    // \brief: rank number
    int rank() const;

    // \brief: number of ranks in the communicator
    int size() const;

    // \brief: duplicate the communicator
    Comm duplicate();

    // \brief: splits the comm in sub comm return the Comm of this rank
    Comm split(int color, int key);

    // \brief: initiates a message to the target rank
    Future send(void *buf, int count, MPI_Datatype datatype,
                int target, int tag = 0);

    // \brief: initiates the receive of a message from target
    Future recv(void *buf, int count, MPI_Datatype datatype,
                int target, int tag = 0);

#if MPI_VERSION >= 3
    // \brief: initiates an AllReduce Operation
    Future all_reduce(void *sendbuf, void* recvbuf, int count,
                      MPI_Datatype datatype, MPI_Op op);
#endif

    // \brief: signal an error with err_code to all other ranks
    void signal_error(int err_code);

    // \brief: just call the raw barrier command
    // without additional error handling
    void raw_barrier();
};

struct ErrorData {
    friend class Future;
    friend class Comm;
 private:
#ifndef MPIX_FT
    MPI_Comm error_comm_;
    MPI_Request error_request_;
#else
    MPI_Comm& error_comm_;
#endif
    int code_buffer;  // buffer for the error request

    // \brief: collective communication to determine failed nodes
    void get_failed_ranks(bool failed, int code,
                          std::vector<int>* failed_ranks,
                          std::vector<int>* error_codes);

 public:
    bool is_corrupted_ = false;
    explicit ErrorData(MPI_Comm* comm);

    ~ErrorData();

    // \brief: agrees with all ranks on an appropriate error state
    void agree_and_throw(bool failed = false,
                         int error_code = 0, bool corrupted = false);

    // \brief: signal an error with err_code to all other ranks
    void signal_error(int err_code, bool corrupted = false);
};
}  // namespace MPICXX

#undef DEBUG

#endif
