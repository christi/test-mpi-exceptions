#ifndef MPICXX_INSTANCE_HH
#define MPICXX_INSTANCE_HH

#include <mpi.h>


#ifdef OPEN_MPI
#include <mpi-ext.h>
#endif
#include <memory>

#include "comm.hh"

#ifdef MPICXX_ENABLE_DEBUGOUTPUT
#include <iostream>
#define DEBUG(A) std::cout << A << std::endl
#else
#define DEBUG(A)
#endif


namespace MPICXX {
class Instance;
class Comm;

const Instance & initialize(int & argc, char** & argv);

/** \brief class representing the global concept of MPI,
    this exists only as a singleton */
class Instance {
    bool valid;
    bool has_initialized;  // is this class responsible of finalization

    // \brief initalizes the MPI instance
    void initialize(int & argc, char** & argv);
    Instance(Instance const&) = delete;
    Instance& operator=(Instance const&) = delete;
 public:
    // \brief returns and stores the singletone instance
    static std::shared_ptr<Instance> & singleton();
    Instance();
    ~Instance();
    static Comm& comm_world();

    /** global method to get access to the MPICXX::Instance singleton */
    friend const Instance & instance();

    /** global method to get access to and initialize the MPICXX::Instance
        singleton */
    friend const Instance & initialize(int & argc, char** & argv);
};

}  // namespace MPICXX

#undef DEBUG

#endif
