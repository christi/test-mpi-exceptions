#ifndef MPICXX_FUTURE_HH
#define MPICXX_FUTURE_HH

#include "comm.hh"

#ifdef MPICXX_ENABLE_DEBUGOUTPUT
#include <iostream>
#define DEBUG(A) std::cout << A << std::endl
#else
#define DEBUG(A)
#endif

namespace MPICXX {

class Comm;

class Future {
    friend class Comm;
    explicit Future(const Comm& comm);
    MPI_Request request_;
    const Comm * comm_;
 public:
    Future();
    ~Future();
    bool wait();
    void cancel();
};
}  // namespace MPICXX

#undef DEBUG

#endif
