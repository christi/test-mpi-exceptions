#ifndef MPICXX_EXCEPTIONS_HH
#define MPICXX_EXCEPTIONS_HH

#include <mpi.h>

#include <vector>
#include <string>
#include <sstream>

#ifdef OPEN_MPI
#include <mpi-ext.h>
#endif

#include "comm.hh"

#ifdef MPICXX_ENABLE_DEBUGOUTPUT
#include <iostream>
#define DEBUG(A) std::cout << A << std::endl
#else
#define DEBUG(A)
#endif

namespace MPICXX {

const std::string errorstring(int errorcode);

class exception {  // interface for MPICXX exceptions
};

// \brief indicates an error on local or remote rank and contains
// error information
class Propagated_exception : public exception {
    friend class ErrorData;
 private:
    std::vector<int> failed_ranks_;
    std::vector<int> error_codes_;
    std::string str;
    Propagated_exception();
    Propagated_exception(std::vector<int> fr, std::vector<int> ec);
 public:
    // \brief returns a vector of failed ranks
    const std::vector<int> failed_ranks() const;

    // \brief returns a vector of the error codes with respect to the
    // failed_ranks
    const std::vector<int> error_codes() const;

    // returns an message
        const char * what();
};

    // \brief indicates a corrupted communicator
class Comm_corrupted_exception : public exception {
 public:
    Comm_corrupted_exception() {}
};

    // \brief indicates an MPI error. This exception is local use
    // Comm::signalError to propagate the exception.
class MPI_error_exception : public std::exception {
    friend class Future;
    friend class Comm;
 private:
    const int error_class_ = MPI_SUCCESS;
    explicit MPI_error_exception(int error_class) : error_class_(error_class) {
    }
 public:
    // \brief: returns a message
    const char* what() const noexcept;

    // \brief returns the MPI error code
    const int error_class() const;
};
}  // namespace MPICXX

#undef DEBUG
#endif
