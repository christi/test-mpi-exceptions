#include "future.hh"

#ifdef MPICXX_ENABLE_DEBUGOUTPUT
#include <iostream>
#define DEBUG(A) std::cout << A << std::endl
#else
#define DEBUG(A)
#endif


namespace MPICXX {

    Future::Future(const Comm& comm)
        : request_(MPI_REQUEST_NULL), comm_(&comm) {}
    Future::Future() : request_(MPI_REQUEST_NULL), comm_(nullptr) {}

    Future::~Future() {
        /* we should cancel the request */
        if (request_ != MPI_REQUEST_NULL) {
            int flag;
            MPI_Request_get_status(request_, &flag, MPI_STATUS_IGNORE);
            if (!flag) {
                DEBUG("Future:\tcancel / free communication request");
                MPI_Cancel(&request_);
                MPI_Request_free(&request_);
            }
        }
    }



#ifndef MPIX_FT
    bool Future::wait() {
        if (request_ == MPI_REQUEST_NULL) {
            DEBUG(comm_->rank_ <<
                  "\t wait called, but request is already complete!");
            return false;
        }
        DEBUG(comm_->rank() << "\twaiting...");
        // we wait until either our request, or an error request finished
        int flag = 0;
        MPI_Status status;
        status.MPI_ERROR = MPI_SUCCESS;
        MPI_Request requests[2] = { request_, comm_->error_->error_request_};
        int index = -1;
        comm_->check_mpi_result(MPI_Waitany(2, requests, &index, &status)
                                , status);
        if (index != 1) {  // check for propagated error_code
            comm_->check_mpi_result(MPI_Test(&comm_->error_->error_request_,
                                             &flag, &status),
                                    status);
        }
        if (index == 1 || flag) {
            comm_->error_->error_request_ = MPI_REQUEST_NULL;
            // wait for all ranks received error
            DEBUG(comm_->rank() << "\twaiting for all ranks...");
            MPI_Barrier(comm_->error_->error_comm_);
            comm_->error_->agree_and_throw();
            DEBUG(comm_->rank() << "\t here i am!");
        }
        // check whether the request was canceled
        comm_->check_mpi_result(MPI_Test_cancelled(&status, &flag));
        if (flag) {
            DEBUG(comm_->rank_ << "\t request was canceled!");
            return false;
        }
        DEBUG(comm_->rank() << "\tget done " << flag << " " <<
              comm_->error_->code_buffer);
        DEBUG(comm_->rank() << "\tsuccess: " << MPI_SUCCESS <<
              "/" << status.MPI_ERROR);
        return true;
    }
#else
    bool Future::wait() {
        if (request_ == MPI_REQUEST_NULL) {
            DEBUG(comm_->rank_ <<
                  "\t wait called, but request is already complete!");
            return false;
        }
        DEBUG(comm_->rank() << "\twaiting...");
        // we wait until either our request, or the error_request finished
        int flag = 0;
        MPI_Status status;
        status.MPI_ERROR = MPI_SUCCESS;
        int result = -1;
        comm_->check_mpi_result(MPI_Wait(&request_, &status), status);
        // check whether we received an error
        comm_->check_mpi_result(MPI_Test_cancelled(&status, &flag));
        if (flag) {
            DEBUG(comm_->rank_ << "\t request was canceled!");
            return false;
        }
        DEBUG(comm_->rank() << "\tget done " << flag << " " <<
              comm_->error_->code_buffer);
        DEBUG(comm_->rank() << "\tsuccess: " << MPI_SUCCESS << "/" <<
              status.MPI_ERROR);
        return true;
    }
#endif

    void Future::cancel() {
        comm_->check_mpi_result(MPI_Cancel(&request_));
    }

}  // namespace MPICXX

#undef DEBUG
