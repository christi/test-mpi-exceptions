#include "comm.hh"

#ifdef MPICXX_ENABLE_DEBUGOUTPUT
#include <iostream>
#define DEBUG(A) std::cout << A << std::endl
#else
#define DEBUG(A)
#endif

namespace MPICXX {

    Comm& Instance::comm_world() {
        static Comm comm_world_(MPI_COMM_WORLD);
        return comm_world_;
    }

    void Instance::initialize(int & argc, char** & argv) {
        int wasInitialized = -1;
        MPI_Initialized(&wasInitialized);
        if (!wasInitialized) {
            MPI_Init(&argc, &argv);
            has_initialized = true;
        } else {
            has_initialized = false;
        }
        valid = true;
        // set initially for COMM_WORLD, other Communicators inherit
        MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN);
    }

    std::shared_ptr<Instance> & Instance::singleton() {
        static auto inst = std::make_shared<Instance>();
        return inst;
    }

    Instance::Instance() : valid(false), has_initialized(false) {}

    Instance::~Instance() {
        int wasFinalized = -1;
        MPI_Finalized(&wasFinalized);
        if (!wasFinalized && has_initialized) {
            int result = MPI_Finalize();
            if (result != MPI_SUCCESS)
                DEBUG("Cant finalize MPI instance!");
        }
    }

    const Instance & instance() {
        auto & inst = Instance::singleton();
        assert(inst->valid);
        return *inst;
    }

    const Instance & initialize(int & argc, char** & argv) {
        auto & inst = Instance::singleton();
        inst->initialize(argc, argv);
        return *inst;
    }
}  // namespace MPICXX

#undef DEBUG
