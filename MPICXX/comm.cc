#include "comm.hh"

#include <iostream>
#include "exceptions.hh"

#ifdef MPICXX_ENABLE_DEBUGOUTPUT
#define DEBUG(A) std::cout << A << std::endl
#else
#define DEBUG(A)
#endif

namespace MPICXX {

    Comm::~Comm() {
        // is this an error?
        if (!error_->is_corrupted_ && std::uncaught_exception()) {
            DEBUG(rank_ << "\tuncaught exception, propagate error via MPI");
            error_->signal_error(1, true);
        }
        // possibly cleanup the communicator
        if (comm_ != MPI_COMM_WORLD && comm_ != MPI_COMM_SELF) {
            int result = MPI_Comm_free(&comm_);
            DEBUG(rank_ << "\tComm:\t free comm: " << result);
        }
    }

    template <typename F, typename... Args>
    Future Comm::async_call(F mpicall, Args... args) {
        Future f(*this);
        check_mpi_result(mpicall(args..., comm_, &f.request_));
        return f;
    }

    void Comm::signal_error(int err_code) {
        error_->signal_error(err_code);
    }

    int Comm::rank() const {
        return rank_;
    }

    int Comm::size() const {
        return size_;
    }
    Comm Comm::duplicate() {
        MPI_Comm c;
        check_mpi_result(MPI_Comm_dup(comm_, &c));
        MPI_Comm_set_errhandler(comm_, MPI_ERRORS_RETURN);
        DEBUG(rank_ << "\tComm:\tComm duplicated");
        return Comm(c);
    }

    Comm Comm::split(int color, int key) {
        MPI_Comm new_comm;
        MPI_Comm_split(comm_, color, key, &new_comm);
        MPI_Comm_set_errhandler(new_comm, MPI_ERRORS_RETURN);
        return Comm(new_comm);
    }

    Future Comm::send(void *buf, int count, MPI_Datatype datatype,
                      int target, int tag) {
        return async_call(MPI_Isend, buf, count, datatype, target, tag);
    }

    Future Comm::recv(void *buf, int count, MPI_Datatype datatype,
                      int target, int tag) {
        return async_call(MPI_Irecv, buf, count, datatype, target, tag);
    }

#if MPI_VERSION >= 3
    Future Comm::all_reduce(void *sendbuf, void* recvbuf, int count,
                            MPI_Datatype datatype, MPI_Op op) {
        return async_call(MPI_Iallreduce, sendbuf, recvbuf, count,
                          datatype, op);
    }
#endif

    Comm::Comm(const MPI_Comm & comm) : comm_(comm),
                                        error_(new ErrorData(&comm_)) {
        check_mpi_result(MPI_Comm_rank(comm, &rank_));
        check_mpi_result(MPI_Comm_size(comm, &size_));
    }

    void Comm::raw_barrier() {
        MPI_Barrier(comm_);
    }

    void Comm::check_mpi_result(int mpi_error_code,
                                const MPI_Status& status) const {
        int mpi_error_class = mpi_error_code;
        // maps error code to an error class defined in MPI standard
        MPI_Error_class(mpi_error_code, &mpi_error_class);
        if (mpi_error_class == MPI_ERR_IN_STATUS) {
            mpi_error_code = status.MPI_ERROR;
            // maps error code to an error class defined in MPI standard
            MPI_Error_class(mpi_error_code, &mpi_error_class);
        }
#ifdef MPIX_FT
        if (mpi_error_class == MPIX_ERR_REVOKED) {
            DEBUG(rank_ << "\tdetect revoked communicator");
            error_->agree_and_throw(false, 0, false);
        }
        if (mpi_error_class == MPIX_ERR_PROC_FAILED ||
           mpi_error_class == MPIX_ERR_PROC_FAILED_PENDING) {
            DEBUG(rank_ << "\tdetect process failure!");
            error_->signal_error(1, true);
            throw(Comm_corrupted_exception());
        }
#endif
        if (mpi_error_code) {
            DEBUG(rank_ << "\tdetect MPI error " <<
                  errorstring(mpi_error_code));
            throw(MPI_error_exception(mpi_error_code));
        }
    }

    ErrorData::ErrorData(MPI_Comm* comm)
#ifdef MPIX_FT
        : error_comm_(*comm)
#endif
    {
        is_corrupted_ = false;
        code_buffer = 0;
#ifndef MPIX_FT
        int result = MPI_Comm_dup(*comm, &error_comm_);
        result |= MPI_Comm_set_errhandler(error_comm_, MPI_ERRORS_RETURN);
        DEBUG("\tErrorData:\tCreated comm");
        result |= MPI_Irecv(&code_buffer, 1, MPI_INT, MPI_ANY_SOURCE,
                            ERROR_TAG, error_comm_, &error_request_);
        if (result)
            throw(std::runtime_error("Initiaization of Error Channel failed"));
#endif
    }

    ErrorData::~ErrorData() {
        // cleanup the error_* members
#ifndef MPIX_FT
        if (error_request_ != MPI_REQUEST_NULL) {
            int flag;
            MPI_Request_get_status(error_request_, &flag, MPI_STATUS_IGNORE);
            if (!flag) {
                int result = MPI_Cancel(&error_request_);
                result |= MPI_Request_free(&error_request_);
                DEBUG("\tErrorData:\tfree error request " << result);
            }
        }
        int result = MPI_Comm_free(&error_comm_);
        DEBUG("\tErrorData:\tfree error comm " << result);
#endif
    }

    void ErrorData::signal_error(int err_code, bool corrupted) {
        DEBUG("Signal error " << err_code);
        code_buffer = err_code;
        is_corrupted_ = corrupted;
#ifndef MPIX_FT
        int rank = 0;
        int size = 0;
        MPI_Comm_rank(error_comm_, &rank);
        MPI_Comm_size(error_comm_, &size);
        int i = 0;
        std::vector<MPI_Request> err_reqs(size-1);
        for (int p = 0; p < size; p++) {
            if (p != rank) {
                DEBUG(rank << "\tsignal error " << code_buffer << " to " << p);
                MPI_Issend(&code_buffer, 1, MPI_INT, p, ERROR_TAG,
                           error_comm_, &(err_reqs[i++]));
            }
        }
        DEBUG(rank << "\tsignal error done");
        // wait for all ranks failed
        MPI_Barrier(error_comm_);
        // cleanup all pending requests
        for (int i = 0; i < size-1; i++) {
            int flag = 0;
            MPI_Test(&(err_reqs[i]), &flag, MPI_STATUS_IGNORE);
            if (!flag) {
                MPI_Cancel(&(err_reqs[i]));
                MPI_Request_free(&(err_reqs[i]));
            }
        }
#else
        DEBUG("\tRevoke communicator");
        MPIX_Comm_revoke(error_comm_);
#endif
        agree_and_throw(true, err_code, corrupted);
    }

    void ErrorData::agree_and_throw(bool failed, int error_code,
                                    bool corrupted) {
        DEBUG("AGREE_AND_THROW: "<< failed
              << ", " << error_code << ", " << corrupted);
        int not_corrupted = corrupted?0:1;  // means that im not corrupted
#ifndef MPIX_FT
        int all_not_corrupted = 0;
        MPI_Allreduce(&not_corrupted, &all_not_corrupted, 1, MPI_INT,
                      MPI_BAND, error_comm_);
        DEBUG("Reduce finish: " << all_not_corrupted);
        not_corrupted = all_not_corrupted;
#else
        MPIX_Comm_agree(error_comm_, &not_corrupted);
        DEBUG("AGREED ON: " << not_corrupted);
#endif
        is_corrupted_ = !not_corrupted;
        if (!not_corrupted) {
            // do not throw if this rank was initially corrupted!
            if (!corrupted)
                throw(Comm_corrupted_exception());
            else
                return;
        }
#ifdef MPIX_FT  // shrink comm to communicate error_data
        MPIX_Comm_failure_ack(error_comm_);
        MPI_Group hard_failed;
        MPIX_Comm_failure_get_acked(error_comm_, &hard_failed);
        int num_hard_failed;
        MPI_Group_size(hard_failed, &num_hard_failed);
        MPI_Group_free(&hard_failed);
        if (num_hard_failed > 0)
            throw(Comm_corrupted_exception());
        MPI_Comm new_comm;
        int result = MPIX_Comm_shrink(error_comm_, &new_comm);
        DEBUG("\tshrink: " << errorstring(result));
        MPIX_Comm_agree(error_comm_, &result);
        if (result != MPI_SUCCESS)
            throw(Comm_corrupted_exception());
        MPI_Comm_free(&error_comm_);
        error_comm_ = new_comm;
#else
        // communicator stays alive, maybe renew the request and
        // complete remaining incoming messages
        int flag = 1;
        if (error_request_ != MPI_REQUEST_NULL) {
            MPI_Test(&error_request_, &flag, MPI_STATUS_IGNORE);
        }
        while (flag) {
            MPI_Irecv(&code_buffer, 1, MPI_INT, MPI_ANY_SOURCE,
                      ERROR_TAG, error_comm_, &error_request_);
            MPI_Test(&error_request_, &flag, MPI_STATUS_IGNORE);
        }
#endif
        auto ex = Propagated_exception();
        get_failed_ranks(failed, error_code, &ex.
                         failed_ranks_, &ex.error_codes_);
        throw(ex);
    }

    void ErrorData::get_failed_ranks(bool failed, int code,
                                     std::vector<int>* failed_ranks,
                                     std::vector<int>* error_codes) {
        DEBUG("\tget failed ranks. Error code: " << code);
        int failed_ = failed != 0?1:0;
        int failed_rank_index = 0;
        int result = MPI_Scan(&failed_, &failed_rank_index, 1, MPI_INT,
                              MPI_SUM, error_comm_);
        int num_failed = failed_rank_index;
        int size = 0;
        int rank = -1;
        MPI_Comm_size(error_comm_, &size);
        MPI_Comm_rank(error_comm_, &rank);
        result |= MPI_Bcast(&num_failed, 1, MPI_INT, size-1, error_comm_);
        failed_ranks->resize(num_failed);
        std::fill(failed_ranks->begin(), failed_ranks->end(), 0);
        error_codes->resize(num_failed);
        std::fill(error_codes->begin(), error_codes->end(), 0);
        if (failed) {
            failed_ranks->at(failed_rank_index-1) = rank;
            error_codes->at(failed_rank_index-1) = code;
        }
        std::vector<int> buf = *failed_ranks;
        result |= MPI_Allreduce(buf.data(), failed_ranks->data(), num_failed,
                                MPI_INT, MPI_MAX, error_comm_);
        buf = *error_codes;
        result |= MPI_Allreduce(buf.data(), error_codes->data(), num_failed,
                                MPI_INT, MPI_MAX, error_comm_);
        if (result != MPI_SUCCESS)
            throw(Comm_corrupted_exception());
    }
}  // namespace MPICXX

#undef DEBUG
