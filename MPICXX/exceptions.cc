#include "exceptions.hh"

#include <mpi.h>
#include <string>
#include <vector>


#ifdef MPICXX_ENABLE_DEBUGOUTPUT
#include <iostream>
#define DEBUG(A) std::cout << A << std::endl
#else
#define DEBUG(A)
#endif

namespace MPICXX {
    const std::string errorstring(int errorcode) {
        int len;
        char msg[MPI_MAX_ERROR_STRING];
        MPI_Error_string(errorcode, msg, &len);
        return msg;
    }

    Propagated_exception::Propagated_exception() {}

    Propagated_exception::Propagated_exception(std::vector<int> fr,
                                               std::vector<int> ec)
            : failed_ranks_(fr)
            , error_codes_(ec) {
        }

    const std::vector<int> Propagated_exception::failed_ranks() const {
        return failed_ranks_;
    }

    const std::vector<int> Propagated_exception::error_codes() const {
        return error_codes_;
    }

    const char * Propagated_exception::what() {
        std::stringstream msg;
        for (int i = 0; i < failed_ranks_.size(); i++) {
            msg << "[rank:  " << failed_ranks_[i] << ", code "
                << error_codes_[i] << "]";
        }
        str = msg.str();
        return str.c_str();
    }

    const char* MPI_error_exception::what() const noexcept {
        return errorstring(error_class_).c_str();
    }

    const int MPI_error_exception::error_class() const {
        return error_class_;
    }
}  // namespace MPICXX

#undef DEBUG
