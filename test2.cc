#define OMPI_SKIP_MPICXX
#define MPICH_SKIP_MPICXX

#include <unistd.h>
#include <iostream>
#include <sstream>
#include <csignal>
#include <chrono>
#include "MPICXX/instance.hh"
#include "MPICXX/comm.hh"

int rank = -1;
std::chrono::time_point<std::chrono::system_clock> start;

// 0=no error
// 1=exception
// 2=hard failure(crash)
// 3=MPI Error
// 4=cancel request
// 5=two ranks throws simultanously
// 6=one ranks died one rank throws
// 7=like 6 but with delay before wait

int main(int argc, char** argv) {
    try {
        auto & mpi = MPICXX::initialize(argc, argv);
        auto& comm_world = mpi.comm_world();
        rank = comm_world.rank();
        comm_world.raw_barrier();
        int errortype = 0;
        if (argc > 1)
            std::istringstream(argv[1]) >> errortype;
        int iterations = 0;
        if (argc > 2)
            std::istringstream(argv[2]) >> iterations;
        for (int i = 0; i < iterations; i++) {
            try {
                try {
                    if (rank == 0) {
                        // start time measurement
                        start = std::chrono::system_clock::now();
                        switch (errortype) {
                        case 1:
                        case 5:
                            throw(std::runtime_error("Don't Panic!"));
                        case 2:
                        case 6:
                        case 7:
                            raise(SIGKILL);  // exit without good bye
                        case 3:
                            {
                                // There is no rank -42
                                auto f = comm_world.send(&errortype, 1, MPI_INT,
                                                         -42);
                                f.wait();
                            }
                            break;
                        case 4:
                            usleep(1000000);
                        case 0:
                        default:
                            MPICXX::Future f[comm_world.size()-1];
                            for (int i = 1; i < comm_world.size(); ++i) {
                                // send msg to all ranks
                                f[i-1] = comm_world.send(&errortype, 1, MPI_INT,
                                                         i);
                            }
                            for (int i = 1; i < comm_world.size(); ++i) {
                                f[i-1].wait();
                            }
                        }
                    }
                    if (rank == comm_world.size()-1 &&
                        (errortype == 5 || errortype == 6))
                        throw(std::domain_error("just another exception"));
                    if (rank != 0) {
                        int buf;
                        auto f = comm_world.recv(&buf, 1, MPI_INT, 0);
                        if (errortype == 4)
                            f.cancel();
                        f.wait();
                    }
                    std::cout << rank << "\texit successfully" << std::endl;
                } catch(MPICXX::MPI_error_exception& e) {
                    std::cout << rank << "\tsome MPI call failed."<< std::endl
                              << "All ranks should leave with error 4711!"
                              << std::endl;
                    comm_world.signal_error(4711);
                } catch(std::domain_error& e) {
                    std::cout << rank << "\tcaught an domain_error."
                              << " All ranks should leave with error 123"
                              << std::endl;
                    comm_world.signal_error(123);
                } catch(std::exception& e) {
                    std::cout << rank << "\tcaught an exception."
                              << " All ranks should leave with error 1337"
                              << std::endl;
                    comm_world.signal_error(1337);
                }
            } catch(MPICXX::Propagated_exception& e) {
                std::cout << rank << "\tPropagated_exception:";
                std::cout << e.what() << std::endl;
            }
            errortype = 0;
            std::cout << rank << "\tIteration: " << i << std::endl;
            comm_world.raw_barrier();
        }
    } catch(MPICXX::Comm_corrupted_exception& e) {
        std::cout << rank << "\tcommunicator is corrupted" << std::endl;
    }
    catch(...) {
        std::cout << rank << "\tuncaught exception" << std::endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);
    if (rank == 0) {
        auto end = std::chrono::system_clock::now();
        auto elapsed = end - start;
        std::cout << static_cast<double>(elapsed.count())/1000000 << std::endl;
    }
    return 0;
}
