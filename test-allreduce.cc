#define OMPI_SKIP_MPICXX
#define MPICH_SKIP_MPICXX

#include <unistd.h>
#include <iostream>
#include <sstream>
#include "MPICXX/instance.hh"
#include "MPICXX/comm.hh"

int main(int argc, char** argv)
try {
    auto & mpi = MPICXX::initialize(argc, argv);
    auto comm_world = mpi.comm_world().duplicate();
    int error_type = 0;
    if (argc > 1)
        std::istringstream(argv[1]) >> error_type;

    int num_iterations = 1;
    if (argc > 2)
        std::istringstream(argv[2]) >> num_iterations;

    int rank = comm_world.rank();
    for (int i = 0; i < num_iterations; i++) {
        try {
        if (comm_world.rank() == 0)
            std::cout << "error_type: " << error_type << std::endl;

        if (rank == 0 && error_type == 1) {
            comm_world.signal_error(42);
        }
        if (rank == 0 && error_type == 2) {
            throw(std::domain_error("don't panic!"));
        }
        // determine size via Allreduce
        int size = -1;
        auto future = comm_world.all_reduce(&rank, &size, 1, MPI_INT, MPI_MAX);
        future.wait();
        std::cout << "Hey! We are " << size+1 << " ranks!" << std::endl;
        } catch(MPICXX::Propagated_exception& e) {
            std::cout << "Received error: " << std::endl << e.what()
                      << std::endl;
        }
    }
} catch(MPICXX::Comm_corrupted_exception& e) {
    std::cout << "Communicator corrupted" << std::endl;
} catch(...) {
    std::cout << "uncaught exception" << std::endl;
    return -1;
}
